const router = require('express').Router();
const clienteController = require('../controller/clienteController');
const ReservaController = require('../controller/reservaController');
const VooController = require('../controller/vooController');
const dbController = require('../controller/dbController');
const AssentoController = require('../controller/assentoController');


//cliente
router.post('/cliente/', clienteController.postCliente);
router.get('/listaCliente/', clienteController.getCliente);
router.get('/listaCliente/:id', clienteController.getClienteById);
router.put('/atualizarCliente/:id', clienteController.updateCliente);
router.delete('/deleteCliente/:id', clienteController.deleteCliente);
router.post('/loginCliente/', clienteController.loginCliente);

//voo
router.post('/Voo', VooController.postVoo);
router.get('/listarVoo', VooController.getVoo);
router.get('/vooById/:numero', VooController.getVooById);
router.put('/atualizarVoo/:numero', VooController.updateVoo);
router.delete('/deletarVoo/:numero', VooController.deleteVoo);

//reserva
router.post('/criarReserva', ReservaController.postReserva);
router.get('/listarReservas', ReservaController.getReserva);
router.get('/reservaById/:id', ReservaController.getReservaById);
router.put('/atualizarReserva/:id', ReservaController.updateReserva);
router.delete('/deletarReserva/:id', ReservaController.deleteReserva);

//assentos
router.post('/criarAssento', AssentoController.postAssento);
router.get('/listarAssentos', AssentoController.getAssento);
router.put('/atualizarAssento/:id', AssentoController.updateAssento);
router.delete('/deletarAssento/:id', AssentoController.deleteAssento);


// Rota para consultar todas as tabelas e suas descrições
router.get("/tabelas", dbController.getTable);


// Rota para consultar apenas os nomes das tabelas
router.get("/nomes", dbController.getTableNames);









module.exports = router;