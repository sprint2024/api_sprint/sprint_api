// Importa o módulo de conexão com o banco de dados
const connect = require("../db/connect");

// Define e exporta a classe assentoController
module.exports = class assentoController {

  // Método estático para criar um novo assento
  static postAssento(req, res) {
    // Extrai os dados do corpo da requisição
    const {
      Voo_Numero,
      NumeroAssento,
      Classe,
      Status
    } = req.body;

    // Verifica se todos os campos obrigatórios foram preenchidos
    if (
      !Voo_Numero ||
      !NumeroAssento ||
      !Classe ||
      !Status
    ) {
      // Retorna uma resposta de erro caso algum campo esteja faltando
      return res.status(400).json({ message: 'Alguns campos foram preenchidos incorretamente.' });
    }

    // Verifica se o status é válido ('Ocupado' ou 'Disponível')
    const statusValido = ['Ocupado', 'Disponível'].includes(Status);

    if (!statusValido) {
      // Retorna uma resposta de erro caso o status seja inválido
      return res.status(400).json({ message: 'Status inválido.' });
    }

    // Executa a query de inserção no banco de dados
    connect.query(
      'INSERT INTO Assentos (Voo_Numero, NumeroAssento, Classe, Status) VALUES (?, ?, ?, ?)',
      [Voo_Numero, NumeroAssento, Classe, Status],
      (error, result) => {
        if (error) {
          // Retorna uma resposta de erro caso ocorra um problema na inserção
          return res.status(500).json({ message: 'Erro ao criar o assento.', error });
        }
        // Retorna uma resposta de sucesso com o ID do novo assento
        res.status(201).json({ message: 'Assento criado com sucesso.', assentoId: result.insertId });
      }
    );
  }

  // Método estático para obter todos os assentos
  static getAssento(req, res) {
    // Executa a query de seleção no banco de dados
    connect.query('SELECT * FROM Assentos', (error, assentos) => {
      if (error) {
        // Retorna uma resposta de erro caso ocorra um problema na consulta
        return res.status(500).json({ message: 'Erro ao buscar a lista de assentos.', error });
      }
      // Retorna a lista de assentos em uma resposta de sucesso
      res.status(200).json({ assentos });
    });
  }

  // Método estático e assíncrono para atualizar um assento
  static async updateAssento(req, res) {
    const assentoId = req.params.id;
    // Extrai os dados do corpo da requisição
    const {
      Voo_Numero,
      NumeroAssento,
      Classe,
      Status
    } = req.body;

    // Verifica se todos os campos obrigatórios foram preenchidos
    if (
      !Voo_Numero ||
      !NumeroAssento ||
      !Classe ||
      !Status
    ) {
      // Retorna uma resposta de erro caso algum campo esteja faltando
      return res.status(400).json({ message: 'Alguns campos foram preenchidos incorretamente.' });
    }

    // Verifica se o status é válido ('Ocupado' ou 'Disponível')
    const statusValido = ['Ocupado', 'Disponível'].includes(Status);

    if (!statusValido) {
      // Retorna uma resposta de erro caso o status seja inválido
      return res.status(400).json({ message: 'Status inválido.' });
    }

    // Define a query de atualização e os valores a serem atualizados
    const query = 'UPDATE Assentos SET Voo_Numero = ?, NumeroAssento = ?, Classe = ?, Status = ? WHERE Assento_ID = ?';
    const values = [Voo_Numero, NumeroAssento, Classe, Status, assentoId];

    // Executa a query de atualização no banco de dados
    connect.query(query, values, (error, result) => {
      if (error) {
        // Retorna uma resposta de erro caso ocorra um problema na atualização
        return res.status(500).json({ message: 'Erro ao atualizar o assento.', error });
      }

      if (result.affectedRows === 0) {
        // Retorna uma resposta de erro caso o assento não seja encontrado
        return res.status(404).json({ message: 'Assento não encontrado.' });
      }

      // Retorna uma resposta de sucesso
      res.status(200).json({ message: 'Assento atualizado com sucesso.' });
    });
  }

  // Método estático e assíncrono para deletar um assento
  static async deleteAssento(req, res) {
    const assentoId = req.params.id;

    if (!assentoId) {
      // Retorna uma resposta de erro caso o ID do assento não seja fornecido
      return res.status(400).json({ message: 'ID do assento não fornecido.' });
    }

    // Define a query de deleção e o ID do assento a ser deletado
    const query = 'DELETE FROM Assentos WHERE Assento_ID = ?';
    const values = [assentoId];

    // Executa a query de deleção no banco de dados
    connect.query(query, values, (error, result) => {
      if (error) {
        // Retorna uma resposta de erro caso ocorra um problema na deleção
        return res.status(500).json({ message: 'Erro ao deletar o assento.', error });
      }

      if (result.affectedRows === 0) {
        // Retorna uma resposta de erro caso o assento não seja encontrado
        return res.status(404).json({ message: 'Assento não encontrado.' });
      }

      // Retorna uma resposta de sucesso
      res.status(200).json({ message: 'Assento deletado com sucesso.' });
    });
  }
};
