const connect = require("../db/connect");

module.exports = class reservaController {
  static postReserva(req, res) {
    const {
      Cliente_ID,
      Voo_Numero,
      AssentosReservados,
      ValorTotal,
      DataReserva,
      HoraReserva
    } = req.body;

    // Verifica se todos os campos obrigatórios foram fornecidos
    if (
      !Cliente_ID ||
      !Voo_Numero ||
      !AssentosReservados ||
      !ValorTotal ||
      !DataReserva ||
      !HoraReserva
    ) {
      return res.status(400).json({ message: 'Alguns campos estão vazios.' });
    }

    // Valida os campos fornecidos
    const valorValido = !isNaN(parseFloat(ValorTotal)) && isFinite(ValorTotal); // Valida se ValorTotal é um número
    const assentosReservadosValido = Number.isInteger(AssentosReservados) && AssentosReservados > 0; // Valida se AssentosReservados é um inteiro positivo
    const dataValida = /^\d{4}-\d{2}-\d{2}$/.test(DataReserva); // Valida o formato da data (YYYY-MM-DD)
    const horaValida = /^\d{2}:\d{2}:\d{2}$/.test(HoraReserva); // Valida o formato da hora (HH:MM:SS)

    // Se alguma validação falhar, retorna um erro
    if (!valorValido || !assentosReservadosValido || !dataValida || !horaValida) {
      return res.status(400).json({ message: 'Alguns campos foram preenchidos incorretamente.' });
    }

    // Insere uma nova reserva no banco de dados
    connect.query(
      'INSERT INTO Reserva (Cliente_ID, Voo_Numero, AssentosReservados, ValorTotal, DataReserva, HoraReserva) VALUES (?, ?, ?, ?, ?, ?)',
      [Cliente_ID, Voo_Numero, AssentosReservados, ValorTotal, DataReserva, HoraReserva],
      (error, result) => {
        if (error) {
          console.error('Erro ao criar a reserva:', error); // Adiciona log para depuração
          return res.status(500).json({ message: 'Erro ao criar a reserva.', error });
        }
        // Retorna uma resposta de sucesso com o ID da nova reserva
        res.status(201).json({ message: 'Reserva criada com sucesso.', reservaId: result.insertId });
      }
    );
  }

  static getReserva(req, res) {
    // Consulta todas as reservas na tabela `Reserva`
    connect.query('SELECT * FROM Reserva', (error, reservas) => {
      if (error) {
        console.error('Erro ao buscar a lista de reservas:', error); // Adiciona log para depuração
        return res.status(500).json({ message: 'Erro ao buscar a lista de reservas.', error });
      }
      // Retorna a lista de reservas
      res.status(200).json({ reservas });
    });
  }

  static getReservaById(req, res) {
    const reservaId = req.params.id;

    if (!reservaId) {
      return res.status(400).json({ message: 'ID da reserva não fornecido.' });
    }

    // Consulta uma reserva específica com base no ID fornecido
    connect.query('SELECT * FROM Reserva WHERE Reserva_ID = ?', [reservaId], (error, reservas) => {
      if (error) {
        console.error('Erro ao buscar a reserva:', error); // Adiciona log para depuração
        return res.status(500).json({ message: 'Erro ao buscar a reserva.', error });
      }

      if (reservas.length === 0) {
        // Retorna um erro se a reserva não for encontrada
        return res.status(404).json({ message: 'Reserva não encontrada.' });
      }

      // Retorna os dados da reserva encontrada
      res.status(200).json({ reserva: reservas[0] });
    });
  }

  static async updateReserva(req, res) {
    const reservaId = req.params.id;
    const {
      Cliente_ID,
      Voo_Numero,
      AssentosReservados,
      ValorTotal,
      DataReserva,
      HoraReserva
    } = req.body;

    // Verifica se todos os campos obrigatórios foram fornecidos
    if (
      !Cliente_ID ||
      !Voo_Numero ||
      !AssentosReservados ||
      !ValorTotal ||
      !DataReserva ||
      !HoraReserva
    ) {
      return res.status(400).json({ message: 'Alguns campos foram preenchidos incorretamente.' });
    }

    // Atualiza uma reserva específica com base no ID fornecido
    const query = 'UPDATE Reserva SET Cliente_ID = ?, Voo_Numero = ?, AssentosReservados = ?, ValorTotal = ?, DataReserva = ?, HoraReserva = ? WHERE Reserva_ID = ?';
    const values = [Cliente_ID, Voo_Numero, AssentosReservados, ValorTotal, DataReserva, HoraReserva, reservaId];

    connect.query(query, values, (error, result) => {
      if (error) {
        console.error('Erro ao atualizar a reserva:', error); // Adiciona log para depuração
        return res.status(500).json({ message: 'Erro ao atualizar a reserva.', error });
      }

      if (result.affectedRows === 0) {
        // Retorna um erro se a reserva não for encontrada
        return res.status(404).json({ message: 'Reserva não encontrada.' });
      }

      // Retorna uma resposta de sucesso
      res.status(200).json({ message: 'Reserva atualizada com sucesso.' });
    });
  }

  static async deleteReserva(req, res) {
    const reservaId = req.params.id;

    if (!reservaId) {
      return res.status(400).json({ message: 'ID da reserva não fornecido.' });
    }

    // Deleta uma reserva específica com base no ID fornecido
    const query = 'DELETE FROM Reserva WHERE Reserva_ID = ?';
    const values = [reservaId];

    connect.query(query, values, (error, result) => {
      if (error) {
        console.error('Erro ao deletar a reserva:', error); // Adiciona log para depuração
        return res.status(500).json({ message: 'Erro ao deletar a reserva.', error });
      }

      if (result.affectedRows === 0) {
        // Retorna um erro se a reserva não for encontrada
        return res.status(404).json({ message: 'Reserva não encontrada.' });
      }

      // Retorna uma resposta de sucesso
      res.status(200).json({ message: 'Reserva deletada com sucesso.' });
    });
  }
};
