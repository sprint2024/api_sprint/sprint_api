// Importa o módulo de conexão com o banco de dados
const connect = require("../db/connect").promise();

// Define e exporta a classe clienteController
module.exports = class clienteController {

  // Método estático e assíncrono para criar um novo cliente
  static async postCliente(req, res) {
    const { cpf, nome, email, telefone, senha, cep } = req.body;

    // Verifica se todos os campos obrigatórios estão preenchidos
    if (!cpf || !nome || !email || !telefone || !senha || !cep) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }

    // Validação do CPF: deve conter exatamente 11 dígitos numéricos
    if (isNaN(cpf) || cpf.length !== 11) {
      return res.status(400).json({ error: "CPF inválido. Deve conter exatamente 11 dígitos numéricos" });
    }

    // Validação do email: deve conter '@'
    if (!email.includes("@")) {
      return res.status(400).json({ error: "Email inválido. Deve conter @" });
    }

    // Validação do telefone: deve seguir o formato (XX) XXXX-XXXX ou (XX) XXXXX-XXXX
    const telefoneRegex = /^\(\d{2}\) \d{4,5}-\d{4}$/;
    if (!telefoneRegex.test(telefone)) {
      return res.status(400).json({ error: "Telefone inválido. Formato esperado: (XX) XXXX-XXXX ou (XX) XXXXX-XXXX" });
    }

    // Validação do CEP: deve conter exatamente 8 dígitos numéricos
    if (cep.length !== 8 || isNaN(cep)) {
      return res.status(400).json({ error: "CEP inválido. Deve conter exatamente 8 dígitos numéricos" });
    }

    const query = `INSERT INTO Cliente (CPF, Nome, Email, Telefone, Senha, CEP) VALUES (?, ?, ?, ?, ?, ?)`;
    const values = [cpf, nome, email, telefone, senha, cep];

    try {
      // Executa a query de inserção no banco de dados
      const [results] = await connect.query(query, values);
      return res.status(201).json({ message: "Cliente cadastrado com sucesso" });
    } catch (err) {
      if (err.code === "ER_DUP_ENTRY") {
        // Tratamento de erro para CPF duplicado
        return res.status(400).json({ error: "CPF já cadastrado" });
      }
      console.error("Erro ao executar a consulta:", err);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método estático e assíncrono para obter todos os clientes
  static async getCliente(req, res) {
    const query = `SELECT * FROM Cliente`;

    try {
      // Executa a query de seleção no banco de dados
      const [results] = await connect.query(query);
      return res.status(200).json({ message: "Obtendo todos os clientes", clientes: results });
    } catch (err) {
      console.error("Erro ao executar a consulta:", err);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método estático e assíncrono para obter um cliente por ID
  static async getClienteById(req, res) {
    const clienteId = req.params.id;
    const query = `SELECT * FROM Cliente WHERE ID = ?`;
    const values = [clienteId];

    try {
      // Executa a query de seleção no banco de dados com base no ID do cliente
      const [results] = await connect.query(query, values);
      if (results.length === 0) {
        return res.status(404).json({ error: "Cliente não encontrado" });
      }
      return res.status(200).json({ message: `Obtendo cliente com ID: ${clienteId}`, cliente: results[0] });
    } catch (err) {
      console.error("Erro ao executar a consulta:", err);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método estático e assíncrono para atualizar um cliente
  static async updateCliente(req, res) {
    const clienteId = req.params.id;
    const { cpf, nome, email, telefone, senha, cep } = req.body;

    // Verifica se todos os campos obrigatórios estão preenchidos
    if (!cpf || !nome || !email || !telefone || !senha || !cep) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }

    const queryGetCPF = "SELECT CPF FROM Cliente WHERE ID = ?";
    const valuesGetCPF = [clienteId];

    try {
      // Executa a query de seleção no banco de dados para obter o CPF atual do cliente
      const [results] = await connect.query(queryGetCPF, valuesGetCPF);
      if (results.length === 0) {
        return res.status(404).json({ error: "Cliente não encontrado" });
      }

      const currentCPF = results[0].CPF;

      // Define a query de atualização com base na comparação do CPF atual
      const query = currentCPF === cpf
        ? "UPDATE Cliente SET Nome = ?, Email = ?, Telefone = ?, Senha = ?, CEP = ? WHERE ID = ?"
        : "UPDATE Cliente SET CPF = ?, Nome = ?, Email = ?, Telefone = ?, Senha = ?, CEP = ? WHERE ID = ?";

      const values = currentCPF === cpf
        ? [nome, email, telefone, senha, cep, clienteId]
        : [cpf, nome, email, telefone, senha, cep, clienteId];

      // Executa a query de atualização no banco de dados
      const [updateResults] = await connect.query(query, values);
      if (updateResults.affectedRows === 0) {
        return res.status(404).json({ error: "Cliente não encontrado" });
      }

      return res.status(200).json({ message: `Cliente atualizado com ID: ${clienteId}` });
    } catch (err) {
      if (err.code === "ER_DUP_ENTRY") {
        // Tratamento de erro para CPF duplicado
        return res.status(400).json({ error: "CPF já cadastrado" });
      }
      console.error("Erro ao executar a consulta:", err);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método estático e assíncrono para deletar um cliente
  static async deleteCliente(req, res) {
    const clienteId = req.params.id;
    const query = `DELETE FROM Cliente WHERE ID = ?`;
    const values = [clienteId];

    try {
      // Executa a query de deleção no banco de dados
      const [results] = await connect.query(query, values);
      if (results.affectedRows === 0) {
        return res.status(404).json({ error: "Cliente não encontrado" });
      }

      return res.status(200).json({ message: `Cliente excluído com ID: ${clienteId}` });
    } catch (err) {
      console.error("Erro ao executar a consulta:", err);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método estático e assíncrono para login de um cliente
  static async loginCliente(req, res) {
    const { email, senha } = req.body;

    // Verifica se os campos obrigatórios de email e senha estão preenchidos
    if (!email || !senha) {
      return res.status(400).json({ error: "Email e senha são obrigatórios" });
    }

    const query = `SELECT * FROM Cliente WHERE Email = ? AND Senha = ?`;
    const values = [email, senha];

    try {
      // Executa a query de seleção no banco de dados para autenticação do cliente
      const [results] = await connect.query(query, values);
      if (results.length === 0) {
        return res.status(401).json({ error: "Credenciais inválidas" });
      }

      return res.status(200).json({ message: "Login realizado com sucesso", cliente: results[0] });
    } catch (err) {
      console.error("Erro ao executar a consulta:", err);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }
};
