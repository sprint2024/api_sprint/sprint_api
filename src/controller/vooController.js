const connect = require("../db/connect");

module.exports = class vooController {
  static postVoo(req, res) {
    const {
      CompanhiaAerea,
      Origem,
      Destino,
      Data_de_ida,
      Data_de_volta,
      Hora,
      Classe,
      Preco,
      AssentosDisponiveis,
    } = req.body;

    // Verifica se todos os campos obrigatórios foram fornecidos
    if (
      !CompanhiaAerea ||
      !Origem ||
      !Destino ||
      !Data_de_ida ||
      !Data_de_volta ||
      !Hora ||
      !Classe ||
      !Preco ||
      !AssentosDisponiveis
    ) {
      return res.status(400).json({ message: 'Alguns campos foram preenchidos incorretamente.' });
    }

    // Validação adicional dos campos
    const precoValido = !isNaN(parseFloat(Preco)) && isFinite(Preco); // Valida se o preço é um número válido
    const assentosValido = Number.isInteger(AssentosDisponiveis) && AssentosDisponiveis > 0; // Valida se os assentos disponíveis são um inteiro positivo
    const dataIdaValida = /^\d{4}-\d{2}-\d{2}$/.test(Data_de_ida); // Valida o formato da data de ida (YYYY-MM-DD)
    const dataVoltaValida = /^\d{4}-\d{2}-\d{2}$/.test(Data_de_volta); // Valida o formato da data de volta (YYYY-MM-DD)
    const horaValida = /^\d{2}:\d{2}:\d{2}$/.test(Hora); // Valida o formato da hora (HH:MM:SS)

    // Se alguma validação falhar, retorna um erro
    if (!precoValido || !assentosValido || !dataIdaValida || !dataVoltaValida || !horaValida) {
      return res.status(400).json({ message: 'Alguns campos foram preenchidos incorretamente.' });
    }

    // Insere um novo voo no banco de dados
    connect.query(
      'INSERT INTO Voo (CompanhiaAerea, Origem, Destino, Data_de_ida, Data_de_volta, Hora, Classe, Preco, AssentosDisponiveis) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)',
      [CompanhiaAerea, Origem, Destino, Data_de_ida, Data_de_volta, Hora, Classe, Preco, AssentosDisponiveis],
      (error, result) => {
        if (error) {
          return res.status(500).json({ message: 'Erro ao criar o voo.', error });
        }
        // Retorna uma resposta de sucesso com o ID do novo voo
        res.status(201).json({ message: 'Seu Voo foi criado com sucesso.', vooId: result.insertId });
      }
    );
  }

  static getVoo(req, res) {
    // Consulta todos os voos na tabela `Voo`
    connect.query('SELECT * FROM Voo', (error, voos) => {
      if (error) {
        return res.status(500).json({ message: 'Erro ao buscar a lista de voos.', error });
      }
      // Retorna a lista de voos
      res.status(200).json({ voos });
    });
  }

  static getVooById(req, res) {
    const vooNumero = req.params.numero;

    if (!vooNumero) {
      return res.status(400).json({ message: 'Número do voo não fornecido.' });
    }

    // Consulta um voo específico com base no número fornecido
    connect.query('SELECT * FROM Voo WHERE Numero = ?', [vooNumero], (error, voos) => {
      if (error) {
        console.error('Erro ao buscar o voo:', error); // Adiciona log para depuração
        return res.status(500).json({ message: 'Erro ao buscar o voo.', error });
      }

      if (voos.length === 0) {
        // Retorna um erro se o voo não for encontrado
        return res.status(404).json({ message: 'Voo não encontrado.' });
      }

      // Retorna os dados do voo encontrado
      res.status(200).json({ voo: voos[0] });
    });
  }

  static async updateVoo(req, res) {
    const vooNumero = req.params.numero;
    const {
      CompanhiaAerea,
      Origem,
      Destino,
      Data_de_ida,
      Data_de_volta,
      Hora,
      Classe,
      Preco,
      AssentosDisponiveis,
    } = req.body;

    // Verifica se todos os campos obrigatórios foram fornecidos
    if (
      !CompanhiaAerea ||
      !Origem ||
      !Destino ||
      !Data_de_ida ||
      !Data_de_volta ||
      !Hora ||
      !Classe ||
      !Preco ||
      !AssentosDisponiveis
    ) {
      return res.status(400).json({ message: 'Alguns campos foram preenchidos incorretamente.' });
    }

    // Validação adicional dos campos
    const precoValido = !isNaN(parseFloat(Preco)) && isFinite(Preco); // Valida se o preço é um número válido
    const assentosValido = Number.isInteger(AssentosDisponiveis) && AssentosDisponiveis > 0; // Valida se os assentos disponíveis são um inteiro positivo
    const dataIdaValida = /^\d{4}-\d{2}-\d{2}$/.test(Data_de_ida); // Valida o formato da data de ida (YYYY-MM-DD)
    const dataVoltaValida = /^\d{4}-\d{2}-\d{2}$/.test(Data_de_volta); // Valida o formato da data de volta (YYYY-MM-DD)
    const horaValida = /^\d{2}:\d{2}:\d{2}$/.test(Hora); // Valida o formato da hora (HH:MM:SS)

    // Se alguma validação falhar, retorna um erro
    if (!precoValido || !assentosValido || !dataIdaValida || !dataVoltaValida || !horaValida) {
      return res.status(400).json({ message: 'Alguns campos foram preenchidos incorretamente.' });
    }

    // Atualiza um voo específico com base no número fornecido
    const query = 'UPDATE Voo SET CompanhiaAerea = ?, Origem = ?, Destino = ?, Data_de_ida = ?, Data_de_volta = ?, Hora = ?, Classe = ?, Preco = ?, AssentosDisponiveis = ? WHERE Numero = ?';
    const values = [CompanhiaAerea, Origem, Destino, Data_de_ida, Data_de_volta, Hora, Classe, Preco, AssentosDisponiveis, vooNumero];

    connect.query(query, values, (error, result) => {
      if (error) {
        console.error('Erro ao atualizar o voo:', error); // Adiciona log para depuração
        return res.status(500).json({ message: 'Erro ao atualizar o voo.', error });
      }

      if (result.affectedRows === 0) {
        // Retorna um erro se o voo não for encontrado
        return res.status(404).json({ message: 'Voo não encontrado.' });
      }

      // Retorna uma resposta de sucesso
      res.status(200).json({ message: 'Voo atualizado com sucesso.' });
    });
  }

  static async deleteVoo(req, res) {
    const vooNumero = req.params.numero;

    if (!vooNumero) {
      return res.status(400).json({ message: 'Número do voo não fornecido.' });
    }

    // Deleta um voo específico com base no número fornecido
    const query = 'DELETE FROM Voo WHERE Numero = ?';
    const values = [vooNumero];

    connect.query(query, values, (error, result) => {
      if (error) {
        console.error('Erro ao deletar o voo:', error); // Adiciona log para depuração
        return res.status(500).json({ message: 'Erro ao deletar o voo.', error });
      }

      if (result.affectedRows === 0) {
        // Retorna um erro se o voo não for encontrado
        return res.status(404).json({ message: 'Voo não encontrado.' });
      }

      // Retorna uma resposta de sucesso
      res.status(200).json({ message: 'Voo deletado com sucesso.' });
    });
  }
};
