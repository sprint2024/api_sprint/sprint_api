const cron = require("node-cron");
const cleanUpReserva = require("./services/cleanUpReserva");

//Agendamento da limpeza para executar diariamente à meia-noite
cron.schedule("0 0 * * * *", async()=>{
    try{
        await cleanUpReserva();
        console.log("Limpeza automática executada");
    }catch(error) {
        console.error("Erro ao executar limpeza automática", error);
    }
})