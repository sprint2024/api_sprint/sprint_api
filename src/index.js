const express = require("express");
const cors = require('cors');
const testConnect = require('./db/testConnect');
require('./cron');
const apiRoutes = require('./routes/routes');

class AppController {
  constructor() {
    this.express = express();
    this.middlewares();
    this.routes();
    this.testConnect = testConnect();
  }

  middlewares() {
    this.express.use(cors());
    this.express.use(express.json());
  }

  routes() {
    this.express.use('/passagem/', apiRoutes);
  }
}

module.exports = new AppController().express;
